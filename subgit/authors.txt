#
# This is SubGit authors mapping file.
# Authors mapping is used to map Subversion committers names to Git committers names and vice versa.
#
# This file uses git-svn format, as described at 'http://www.kernel.org/pub/software/scm/git/docs/git-svn.html'
# and consists of the lines in the following format:
#
# svnUser = Git User <user@example.com>
#
user1 = USER1 <1-user1@users.noreply.git.example.com>
user2 = USER2 <2-user2@users.noreply.git.example.com>
