#!/bin/bash -xv
KEY=( $(echo -n 83 | sha256sum) )

subgit register --key "${PWD}/subgit.key" '/var/opt/gitlab/git-data/repositories/@hashed/'*/*"/${KEY}.git"
