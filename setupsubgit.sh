#!/bin/bash -xv

# Information about post-receive:
#
# https://issues.tmatesoft.com/issue/SGT-1267#comment=60-9220
#
# However, it is better to add custom_hooks/user-post-receive (afte
#
# In subgit/config set following in [svn] section
#
# Create pipeline trigger token (Settings > CI/CD - Pipeline trigger tokens
# - Add new token
# - Use token in *.git/custom_hooks/user-post-receive script
#
# ```
# !/bin/bash
#
# token='<gitlab-access-token>'
# id='<project-id>'
#
# while read -a args; do
#     curl --request POST --header "PRIVATE-TOKEN: ${token}" http://localhost/api/v4/projects/${id}/pipeline?ref=${args[2]}
# done
#
# ```

ORGDIR="$(realpath "$PWD")"

if [ "$(whoami)" == "git" ] ; then
    SUDO=""
else
    SUDO="sudo -u git"
fi

TEST=1          # By default, test
DOCONFIGURE=0   # By default, skip configuration
ID=83

TEST=1          # Do operations in test directory
DOCONFIGURE=1   # Do git repo configuration

SVN=svn://example.com/example
CONFIG_FILE=${ORGDIR}/subgit/config.${ID}
if [ -r "${CONFIG_FILE}" ] ; then
 SVN=$(grep "url = svn" "${CONFIG_FILE}")
 SVN=${SVN##*= }
 SVN=${SVN% *}
fi
echo "$SVN"
KEY=$(echo -n $ID | sha256sum)
KEY=${KEY%% *}

GITDIR='/var/opt/gitlab/git-data/repositories/@hashed/'
PARENTDIR=$(realpath "$(echo "${GITDIR}"*/*"/${KEY}.git/..")")
# echo $PARENTDIR ; exit

if [ "$TEST" == 1 ] ; then
    # Test install/import
    SUDO=""
    PARENTDIR=/tmp/TEST.DIR
    # When TEST is active, we delete the target directory,
    # so configuration is necessary.
    DOCONFIGURE=1
    mkdir -p "$PARENTDIR"
    (
        cd "$PARENTDIR" || exit
        rm -rf "$KEY.git"
        git init --bare "$KEY.git"
    )
fi

(
    cd "$PARENTDIR" || exit

    if [ "$DOCONFIGURE" == 1 ] ; then
        # Configure step

        #git init --bare $KEY.git
        $SUDO subgit configure "${SVN}" "$KEY.git"
        # Configure subgit
	TMPSUBGIT=/tmp/subgit
	rm -rf "${TMPSUBGIT}"

        $SUDO cp -p "${CONFIG_BASE}" "$KEY.git/subgit/config"

	cp -Rp "${ORGDIR}/subgit" "${TMPSUBGIT}"
        $SUDO cp -p "${TMPSUBGIT}/authors.txt" "$KEY.git/subgit/authors.txt"
	CONFIG_BASE=${TMPSUBGIT}/config
        if [ -r "${CONFIG_BASE}.$ID" ] ; then CONFIG_BASE="${CONFIG_BASE}.$ID" ; fi
        $SUDO cp -p "${CONFIG_BASE}"  "$KEY.git/subgit/config"
        $SUDO perl -p -i -e "s@url = svn.*@url = $SVN@" "$KEY.git/subgit/config"
        $SUDO cp -p "${TMPSUBGIT}/passwd" "$KEY.git/subgit/passwd"
        if [ -r "${TMPSUBGIT}/passwd.${ID}" ] ; then
            $SUDO cp -p "${TMPSUBGIT}/passwd.${ID}" "$KEY.git/subgit/passwd"
	fi

	rm -rf "${TMPSUBGIT}"
    fi
    #strace -ff -e trace=file \
        #
    #subgit install --rebuild $SVN $KEY.git
    $SUDO subgit install --rebuild "$SVN" "$KEY.git"
    chmod 2700 "$KEY.git"
)
# Test clone
TEST_CLONE=test_clone
rm -rf "${TEST_CLONE}"
git clone "$PARENTDIR/$KEY.git" "${TEST_CLONE}"
(
  cd "${TEST_CLONE}" || exit
  git fetch --all
  git pull --all
  git branch -r
)
