# Setup Subgit svn to git sync on gitlab

This supposes access to the gitlab server OS itself.

## setupsubgit.sh

Helps setup the synchronisation between a svn repository and a git repository on gitlab.

You'll need to:

- Set the project ID in the script;
- Modify TEST and DOCONFIGURE from 1 to 0 as you go;
- Prepare a config.ID and passwd.ID file (or update the default files without the .ID).
  The SVN repository is taken from the config file.

I do this in 2 phase:

1. TEST
   Prepare config.ID and passwd.ID files as descript
   During test, a git repository is initialised and synced with in `/tmp`.
   This is then cloned in `test_clone`.
   Manually check the clone (branches, tags, etc).
   If needed, update the configuration and re-test.
   When happy, "EXECUTE" (below)
   (DOCONFIGURE=0 was mainly needed while preparing the script).


2. EXECUTE
   Setting `TEST=0` and leaving `DOCONFIGURE=1`.

## registerSubgitLicense.sh

Helps register the subgit license on a git repository.

Update the ID in the script to match the gitlab project ID

subgit.key must contain your key.
